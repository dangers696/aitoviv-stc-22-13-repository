import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args)
    {
        System.out.println("Введите текст");
        Scanner in = new Scanner(System.in);
        String string = in.nextLine();
        String[] words = string.split(" ");
        HashMap<String, Integer> NumberOfWords = new HashMap<>();
        for (String word: words)
        {
            if (!NumberOfWords.containsKey(word))
            {
                NumberOfWords.put(word, 0);
            }
            NumberOfWords.put(word, NumberOfWords.get(word) + 1);
        }
        for (String word : NumberOfWords.keySet())
        {
            System.out.println("(*) " + word + " =" + NumberOfWords.get(word));
        }
    }
}